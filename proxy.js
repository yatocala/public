var PROXY = 'SOCKS5 127.0.0.1:18081';

var HOSTS = [
    // google
    'google.com', 'google.ca', 'gmail.com',  'googleapis.com',
    'chrome.com', 'blogspot.com', 'appspot.com', 'ytimg.com', 'youtube.com', 'googlevideo.com',
    'recaptcha.net', 'goo.gl', 'g.co', 'googlezip.net', 'youtu.be', 'gvt1.com',
    'gstatic.com', 'googleusercontent.com', 'ggpht.com', 'icloud.com', 'bit.ly', 'nordcdn.com', 'yandex.com',

    // microsoft
    'xgpuweb.gssv-play-prod.xboxlive.com', 'onedrive.live.com', 'gitlab.com',

    // dev tools 部分可以获取到google ip
    'googlecode.com', 'googlesource.com', 'gcr.io', 'android.com', 'golang.org', 'chromium.org', 'js.org', 'api.fast.com', 'oca.nflxvideo.net',
    'oraclecloud.com', 'getunleash.io', 'page.link', 'admob.com', 'cloudinary.com', 'azureedge.net', 'cheat.sh', 'coroot.com', 'bing.com', 'poe.com', 'apkmirror.com', 'archive.org', 'onetrust.com',
    'feijiji.com', 'civitai.com', 'duckduckgo.com', 'datadoghq.com', 'medium.com', 'about.gitlab.com', 'assets.gitlab-static.net', 'linuxserver.io',
    'id.heroku.com', 'dashboard.heroku.com', 'github.io', 'github.dev', 'gorm.io', 'scylladb.com', 'slideshare.net', 'ycombinator.com', 'openfaas.com', 'uptodown.com', 'figma.com', 'docker.com', 'huggingface.co', 'squarespace.com',
    'hcaptcha.com', 'github.blog', 'githubassets.com', 'objects-origin.githubusercontent.com', 'githubusercontent.com', 'github.com', 'raw.githubusercontent.com', 'hostloc.com', 'soketi.app', 'huggingface.co', 'operatorhub.io', 'cdn.jsdelivr.net', 'imgur.com',
    'ant.design', 'cube.dev', 'steamcommunity.com',

    // 社交类
    'm.me', 'fb.com', 'm.me', 'facebook.com', 'messager.com', 'fbsbx.com', 'fbcdn.net', 'wp.com', 'instagram.com', 'reddit.com',
    'twitter.com', 't.co', 'twitpic.com', 'twimg.com', 'tweetdeck.com', 'twimg.com', 'tumblr.com', 'netflix.com', 
    'disqus.com', 'zh.wikipedia.org', 'dw.com', 'telegram.org', 'v2ex.com', 'cdn.statically.io', 'nyaa.si', 'amazon.co.jp',
    'epicgames.com', 'openai.com', 'steampowered.com', 'oculuscdn.com', 'www.paypalobjects.com', 'oculus.com', 'meta.com', 't.me', "telegram.me", "telegram.org", "telegram-cdn.org", 'brave.com', 'mikanani.me',

    // coins 
    'gate.io', 'huobi.vc', 'huobi.pro', 'huobi.com', 'zb.com', 'kucoin.com', 'coincola.com', 'paxful.com', 'okx.com', 'huobi.co.jp', 'coinex.com',
    // coins dev 
    'tron.network', 'big.one', 'mdex.com', 'mdex.co', 'bnbstatic.com', 

    // amazon
    'amazon.com', 'amazonaws.com',

    // dropbox
    'dropbox.com', 'dropboxusercontent.com', 'db.tt', 'dropbox-dns.com', 'dropboxstatic.com', 

    // coin
    'bitcointalk.org', 'zb.com', 'xmrchain.net', 'mymonero.com', 'supportxmr.com', 'binance.com', 'kucoin.com',

    // cdn
    'cloudfront.net', 'fastly.net', 'netdna-ssl.com', 'cdn.digitaloceanspaces.com', 
    // cdn media
    'highwinds-cdn.com', 'top', 'cdn.dribbble.com', 'donmai.us', 'character.ai'
];

var IGNORE = [
    // 本地地址
    'local', 'test', 'dl.google.com', 'google.cn', 'dewu.net', 'home.io'
]

var HOST_KEYWORDS = [

];

var URL_KEYWORDS = [
    'google', 'facebook', 'twitter', 'torrent', 'proxy', 'vpn'
];


var BLOCK_HOSTS = cleanHosts(HOSTS);
var IGNORE_HOSTS = cleanHosts(IGNORE);


function inHosts(host, hosts) {
    var hostParts = host.split('.'), testHost = [];
    while (hostParts.length) {
        testHost.unshift(hostParts.pop());
        if (hosts[testHost.join('.')]) {
            return true;
        }
    }
}

function inKeywords(uri, keywords) {
    for (var i = 0; i < keywords.length; i++) {
        if (uri.indexOf(keywords[i]) >= 0) {
            return true;
        }
    }
}

function cleanHosts(hosts) {
    var r = {};
    for (var i = 0; i < hosts.length; i++) {
        if (hosts[i][0] !== '!')
            r[hosts[i]] = true;
    }
    return r;
}

function FindProxyForURL(url, host) {
    if (inHosts(host, IGNORE_HOSTS)) {
        return 'DIRECT';
    }

    if (inHosts(host, BLOCK_HOSTS) || inKeywords(url, URL_KEYWORDS)) {
        return PROXY;
    }

    return 'DIRECT';
}
